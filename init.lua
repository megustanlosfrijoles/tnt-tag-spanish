local S = minetest.get_translator("tnttag")

tnttag = {}
tnttag.saved_huds = {}

tnttag.player_speed = minetest.settings:get("tnttag.player_speed") or 3
tnttag.player_jump = minetest.settings:get("tnttag.player_jump") or 1.2
tnttag.player_speed_tagged = minetest.settings:get("tnttag.player_speed_tagged") or 3
tnttag.player_jump_tagged = minetest.settings:get("tnttag.player_jump_tagged") or 1.2

arena_lib.register_minigame("tnttag", {
	prefix = "[tnttag] ",
    icon = "tnttag.png",
	show_minimap = true,
	properties = {
		autochoosewaves = false, -- If true let waves be
		max_players_in_autochoose = 1,--only important for autochoosewaves
		waves = 5, --How many waves needed. Warning at least one more player then waves are needed. Don't work if autochoosewaves is true!
		wavetime = 30,
		pause_length = 5, -- set to 0 for none
	},
	temp_properties = {
		current_wave = 0,
		original_player_amount = 0,
		pause = true,
	},
	player_properties = {
    	tagged = false,
		tnt_if_tagged,
	},
	in_game_physics = {
		speed = tnttag.player_speed,
		jump = tnttag.player_jump,
	},
	hotbar = {
      slots = 1,
      background_image = "tnttag_gui_hotbar.png"
    },
	disabled_damage_types = {"fall", "punch"},--drown? node_damage?
	celebration_time = 10,
	time_mode = "decremental",
})

dofile(minetest.get_modpath("tnttag") .. "/api.lua")
dofile(minetest.get_modpath("tnttag") .. "/items.lua")
dofile(minetest.get_modpath("tnttag") .. "/entities.lua")



-- Thankyou to chmodsayshello ↑

dofile(minetest.get_modpath("tnttag") .. "/hud.lua")
dofile(minetest.get_modpath("tnttag") .. "/auto.lua")

minetest.register_privilege("tnttag_admin", S("Needed for tnttag"))
