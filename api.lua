local S = minetest.get_translator("tnttag")

-- gives a player the tagging item
function tnttag.add_tager(player)
	player:get_inventory():set_stack("main", 1, tnttag.tagitem)
end

-- takes the tagging item
function tnttag.remove_tager(player)
	local inv = player:get_inventory()
	local stack = ItemStack(tnttag.tagitem)
	local taken = inv:remove_item("main", stack)
end

-- adds the tnt head entity
function tnttag.add_tnthead(p_name)
	local player = minetest.get_player_by_name(p_name)
	if player then
		local tnthead = minetest.add_entity(minetest.get_player_by_name(p_name):get_pos(), "tnttag:tnt_if_tagged", nil)
		tnthead:set_attach(minetest.get_player_by_name(p_name), "Head", {x=0, y=10, z=0})
	end
end
-- removes the tnt entity
function tnttag.remove_tnthead(p_name)
	local player = minetest.get_player_by_name(p_name)
	if player then
		local children = player:get_children()
		for _,child in ipairs(children) do
			if child:get_luaentity() and child:get_luaentity().name == "tnttag:tnt_if_tagged" then
				child:set_detach()
                child:remove()
			end
		end
	end
end

-- called when tagged
function tnttag.tagplayer(p_name, arena)
	local player = minetest.get_player_by_name(p_name)
	arena.players[p_name].tagged = true
	tnttag.add_tager(minetest.get_player_by_name(p_name))
    tnttag.add_tnthead(p_name)
	player:set_physics_override({
        speed = tnttag.player_speed_tagged,
		jump = tnttag.player_jump_tagged,
    })
end

-- called when a player tags another player
function tnttag.untagplayer(p_name, arena)
	local player = minetest.get_player_by_name(p_name)
	arena.players[p_name].tagged = false
	tnttag.remove_tager(minetest.get_player_by_name(p_name))
    tnttag.remove_tnthead(p_name)
	player:set_physics_override({
        speed = tnttag.player_speed,
		jump = tnttag.player_jump,
    })
end

-- get the get tagstatus
function tnttag.gettagstatus(p_name, arena)
	return arena.players[p_name].tagged
end

-- Thankyou to chmodsayshello ↓
function tnttag.get_new_tagger_count(arena, num_tagger)
	local players = {}
	local already_tagged = {}
	local players_in_arena = {}
	local count = 0
    for p_name in pairs(arena.players) do
        count = count + 1
		table.insert(players_in_arena, p_name)
    end
    for i = 1,num_tagger do
		local possible_player_index
		local success = false
        while not success do
            possible_player_index = math.random(1,count)
            success = true
            for used in pairs(already_tagged) do
                if possible_player_index == used then
                    success = false
                end
            end
        end
        table.insert(already_tagged,possible_player_index)
        table.insert(players,players_in_arena[possible_player_index])
    end
    return players
end


function tnttag.get_new_tagger(arena, percentage) --same usage: get_new_tagger(arena, 0.25) for 25%
    local count = 0
    for p_name in pairs(arena.players) do
        count = count + 1
    end
    local num_tagger = math.floor(count*percentage)
	if num_tagger < 1 then
		num_tagger = 1
	end
    return tnttag.get_new_tagger_count(arena, num_tagger)
end


function tnttag.explode_player(p_name, arena)
	minetest.sound_play("tnt_explode", {object = minetest.get_player_by_name(p_name), gain = 1.0, max_hear_distance = 128, loop = false,}, true)
	arena_lib.HUD_send_msg("title", p_name, S("You exploded!"),1, nil--[[sound↑]], 0xFF3300)
	arena_lib.remove_player_from_arena(p_name, 1)
	for pl_name,_ in pairs(arena.players) do
		minetest.chat_send_player(pl_name,p_name.." "..S("exploded!"))
	end
end


function tnttag.newwave(arena)
	local player
	local new_tagger
	local real_new_tagger = {}
	arena.current_wave = arena.current_wave+1
	tnttag.update_wave_counter_hud(arena)
	local p_count = 0
	for _ in pairs(arena.players) do
		p_count = p_count + 1
	end
	if arena.current_wave == arena.waves then --Last wave
		new_tagger = tnttag.get_new_tagger(arena, 0.5)
	else
		new_tagger = tnttag.get_new_tagger(arena, 0.5)
	end
	local count = 0
	for _, p_name in pairs(new_tagger) do
		if p_count-count > (arena.waves-arena.current_wave)+1 then --für die bessere lessbarkeit/for better readability
			tnttag.tagplayer(p_name, arena)
			table.insert(real_new_tagger, p_name)
			count = count + 1
		end
	end
	for p_name in pairs(arena.players_and_spectators) do
        local message = S("The new tagger is").." "
        if #real_new_tagger > 1 then
            message = S("The new taggers are").." "
        end
		minetest.chat_send_player(p_name, message .. table.concat(real_new_tagger,", ").."!")
		minetest.sound_play("new_round", {to_player = p_name, gain = 1.0, loop = false,}, true)
	end
	for p_name in pairs(arena.players) do
		player = minetest.get_player_by_name(p_name)
		player:set_pos(arena_lib.get_random_spawner(arena, arena.players[p_name]))--WARNING: Tagged and Untagged player on same spawn-point
    end
	arena.pause = false
	tnttag.update_pause_hud(arena)
end
