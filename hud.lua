function tnttag.generate_HUD(arena, p_name)
	local player = minetest.get_player_by_name(p_name)
	local background_player_count_
	local player_count_
	local background_timer_
	local timer_
	local background_wave_counter_
	local wave_counter_
	local hud_pause_
	local pos_x = 0.48
    local pos_y = 0
    local scale = 3.5
    local distance_x = 80
    local background_width = 32*scale

	background_player_count_ = player:hud_add({
        hud_elem_type = "image",
        position  = {x = pos_x, y = pos_y},
        offset = {x = -distance_x, y = 50},
        text      = "hud_tnttag_player.png",
        alignment = {x = 1.0},
        scale     = {x = scale, y = scale},
        number    = 0xdff6f5,
    })
	player_count_ = player:hud_add({
        hud_elem_type = "text",
        position  = {x = pos_x, y = pos_y},
        offset = {x = -distance_x+37, y = 50+3},
        text      = tostring(arena.players_amount) .. "/" .. tostring(arena.original_player_amount),
        alignment = {x = 0},
        scale     = {x = 100, y = 100},
        number    = 0xdff6f5,
    })

	background_wave_timer_ = player:hud_add({
		hud_elem_type = "image",
		position  = {x = pos_x, y = pos_y},
		offset = {x = background_width/2, y = 50},
		text      = "hud_tnttag_timer.png",
		alignment = {x = 1.0},
		scale     = {x = scale, y = scale},
		number    = 0xdff6f5,
	})
	wave_timer_ = player:hud_add({
		hud_elem_type = "text",
		position  = {x = pos_x, y = pos_y},
		offset = {x = background_width/2 + 37, y = 50+3},
		text      = "-",
		alignment = {x = 0},
		scale     = {x = 100, y = 100},
		number    = 0xdff6f5,
	})

	background_wave_counter_ = player:hud_add({
		hud_elem_type = "image",
		position  = {x = pos_x, y = pos_y},
		offset = {x = distance_x+background_width, y = 50},
		text      = "hud_tnttag_wave.png",
		alignment = {x = 1.0},
		scale     = {x = scale, y = scale},
		number    = 0xdff6f5,
	})
	wave_counter_ = player:hud_add({
        hud_elem_type = "text",
        position  = {x = pos_x, y = pos_y},
        offset = {x = distance_x+background_width+37, y = 50+3},
        text      = tostring(arena.current_wave).."/"..tostring(arena.waves),
        alignment = {x = 0},
        scale     = {x = 100, y = 100},
        number    = 0xdff6f5,
    })

	hud_pause_ = player:hud_add({
		hud_elem_type = "image",
        position  = {x = pos_x, y = pos_y},
		offset = {x = -distance_x*2.7, y = 50},
		text      = "hud_tnttag_pause.png",
		alignment = {x = 1.0},
		scale     = {x = scale, y = scale},
		number    = 0xdff6f5,
	})

	tnttag.saved_huds[p_name] = {
		background_player_count = background_player_count_,
		player_count = player_count_,
		background_wave_timer = background_wave_timer_,
		wave_timer = wave_timer_,
		background_wave_counter = background_wave_counter_,
        wave_counter = wave_counter_,
		hud_pause = hud_pause_,
    }
end

function tnttag.remove_HUD(arena, p_name)
    local player = minetest.get_player_by_name(p_name)

    if not tnttag.saved_huds[p_name] then return end

    for name, id in pairs(tnttag.saved_huds[p_name]) do
        if type(id) == "table" then id = id.id end
        player:hud_remove(id)
    end

    tnttag.saved_huds[p_name] = {}
end

function tnttag.update_player_count_hud(arena)
	for p_name in pairs(arena.players_and_spectators) do
		local player = minetest.get_player_by_name(p_name)
		player:hud_change(tnttag.saved_huds[p_name].player_count, "text", tostring(arena.players_amount) .. "/" .. tostring(arena.original_player_amount))
	end
end

function tnttag.update_wave_timer_hud(arena)
	for p_name in pairs(arena.players_and_spectators) do
		local player = minetest.get_player_by_name(p_name)
		player:hud_change(tnttag.saved_huds[p_name].wave_timer, "text", arena.current_time%arena.wavetime)
	end
end

function tnttag.update_wave_counter_hud(arena)
	for p_name in pairs(arena.players_and_spectators) do
		local player = minetest.get_player_by_name(p_name)
		player:hud_change(tnttag.saved_huds[p_name].wave_counter, "text", tostring(arena.current_wave).."/"..tostring(arena.waves))
	end
end

function tnttag.update_pause_hud(arena)
	if arena.pause then
		for p_name in pairs(arena.players_and_spectators) do
			local player = minetest.get_player_by_name(p_name)
			player:hud_change(tnttag.saved_huds[p_name].hud_pause, "text", "hud_tnttag_pause.png")
		end
	else
		for p_name in pairs(arena.players_and_spectators) do
			local player = minetest.get_player_by_name(p_name)
			player:hud_change(tnttag.saved_huds[p_name].hud_pause, "text", "hud_tnttag_progress.png")
		end
	end
end
